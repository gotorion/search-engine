#include "WebPageQuery.h"

#include <math.h>

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <nlohmann/json.hpp>
#include <regex>
#include <set>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include "CppJieba.h"

using std::getline;
using std::ifstream;
using std::istringstream;
using std::regex;
using std::regex_replace;
using json = nlohmann::json;

WebPageQuery::WebPageQuery()
// : _wordCutTool(JiebaTool::getInstance()) {
{
    // 载入 网页库
    loadPageLib();

    // 加载 倒排索引库
    loadWeightLibrary();

    // 加载停用词
    loadStopWords();
}

// 加载网页库, 将其存入 _pageLib, 文章id 作为 键, 保存文章的 vector 作为 值
void WebPageQuery::loadPageLib() {
    _pageLib.clear();

    // ifstream ifs("../../data/ripepage.dat");
    ifstream ifs(Configuration::getInstance()->getConfigMap()["WEB_PAGE_LIB"]);
    if (!ifs.good()) {
        perror("loadPageLib_ifs");
        exit(1);
    }

    int i = 1;
    while (1) {
        string line;
        // <doc>
        getline(ifs, line);
        if (line.size() == 0) {
            break;
        }

        // <id>...</docid>
        getline(ifs, line);
        _pageLib[i] = {};

        // <url>...</url>
        getline(ifs, line);
        line = line.substr(10);
        line = line.substr(0, line.size() - 6);
        _pageLib[i].push_back(line);

        // <title>...</title>
        getline(ifs, line);
        line = line.substr(12);
        line = line.substr(0, line.size() - 8);
        _pageLib[i].push_back(line);

        // <description>...</description>
        // 读取描述, 因为 描述可能占多行, 需要循环读取
        string description;
        getline(ifs, line);
        line = line.substr(18);
        while (1) {
            // 该行长度小于 14, 则 肯定不是 description
            if (line.size() < 14) {
                description += line;
                getline(ifs, line);
                continue;
            }

            // 长度大于 14, 则判断 末尾是不是 </description>
            string end = line.substr(line.size() - 14);
            if (end == "</description>") {
                line = line.substr(0, line.size() - 14);
                description += line;
                break;
            } else {
                description += line;
            }
            getline(ifs, line);
        }
        _pageLib[i].push_back(description);

        // </doc>
        getline(ifs, line);
        ++i;
    }

    ifs.close();

    // cout << "网页库 载入完成, 共载入 " << _pageLib.size() << " 篇文章" <<
    // endl;
}

// 加载 倒排索引库 文件
void WebPageQuery::loadWeightLibrary() {
    _invertIndexLib.clear();

    // ifstream ifs("../../data/invertindex.dat");
    ifstream ifs(
        Configuration::getInstance()->getConfigMap()["WEB_INVERT_LIB"]);
    if (!ifs.good()) {
        perror("loadLibrary_ifs");
        exit(1);
    }

    string line;
    while (getline(ifs, line)) {
        istringstream iss(line);
        string word;
        iss >> word;

        _invertIndexLib.insert({word, {}});
        _dfLib[word] = 0;

        int id;
        double weight;
        while (iss >> id >> weight) {
            // _invertIndexLib[word].insert({id, weight});
            _invertIndexLib[word][id] = weight;
            _dfLib[word]++;
        }
    }

    ifs.close();

    // cout << "倒排索引库 载入完成, 共载入 " << _invertIndexLib.size() << "
    // 个索引" << endl;
}

// 加载 停用词文件, 存入 unorder_map
void WebPageQuery::loadStopWords() {
    _stopWords.clear();

    // ifstream ifs("../../data/stop_words_zh.txt");
    ifstream ifs(
        Configuration::getInstance()->getConfigMap()["WEB_STOP_WORD_PATH"]);
    if (!ifs.good()) {
        perror("load stop words_zh");
        exit(1);
    }

    string words;
    while (ifs >> words) {
        _stopWords.insert(words);
    }
    ifs.close();
}

// 分词, 并把结果存入 _queryWords
void WebPageQuery::cut(const string& sentence) {
    _queryWords.clear();

    // 句子 分词, 并去除停用词, 将结果存入 _queryWords
    // vector<string> queryWord = _wordCutTool.cut(sentence);
    vector<string> queryWord = JiebaTool::getInstance().cut(sentence);

    for (auto& elem : queryWord) {
        if (_stopWords.find(elem) == _stopWords.end()) {
            _tmpQueryWords.push_back(elem);
            _queryWords.insert(elem);
        }
    }
}

// 求 包含全部查询词的 文章Id
void WebPageQuery::getIntersectionId() {
    _intersectionId.clear();

    // 求并集 的 预处理文件
    // 文章 id : 单词出现个数
    unordered_map<int, int> result;

    for (auto& elem : _queryWords) {
        auto it = _invertIndexLib.find(elem);
        if (it == _invertIndexLib.end()) {
            continue;
        }

        auto idWeightSet = it->second;
        for (auto& elem : idWeightSet) {
            int id = elem.first;
            if (result.find(id) == result.end()) {
                result[id] = 1;
            } else {
                result[id]++;
            }
        }
    }

    // 将所有带查询单词 都包含的 文章Id 存入 _intersectionId
    int wordsNum = _queryWords.size();
    for (auto& elem : result) {
        if (elem.second == wordsNum) {
            _intersectionId.push_back(elem.first);
        }
    }

    // 打印 intersectionId
    // cout << "intersectionId: ";
    // for (auto& elem : _intersectionId) {
    //     cout << elem << " ";
    // }
    // cout << endl;
}

// 计算查询词的权重, 存入 _queryWeight
void WebPageQuery::getQueryWordsWeight() {
    _queryWeight.clear();

    // 记录 w1^2 + w2^2 + ... + wn^2 的和
    double sumw2 = 0;

    for (auto& elem : _queryWords) {
        double idf = 0;
        auto it = _dfLib.find(elem);

        if (it == _dfLib.end()) {
            idf = log2(_pageLib.size() / 1);
        } else {
            idf = log2(_pageLib.size() / (_dfLib[elem] + 1));
        }

        // w = tf * idf, 此处 tf 取 1
        double TF =
            std::count(_tmpQueryWords.begin(), _tmpQueryWords.end(), elem);
        double w = TF * idf;

        _queryWeight.push_back(w);
        sumw2 += w * w;
    }

    for (auto& elem : _queryWeight) {
        elem /= sqrt(sumw2);
    }

    // cout << "查询词权重: ";
    // for (auto& elem : _queryWeight) {
    //     cout << elem << " ";
    // }
    // cout << endl;
}

// 获取 _intersectionId 中各篇文章的 特征向量
void WebPageQuery::getPageWeight() {
    _pageWeight.clear();

    for (auto& id : _intersectionId) {
        _pageWeight[id] = {};
        for (auto& word : _queryWords) {
            double weight = _invertIndexLib[word][id];
            _pageWeight[id].push_back(weight);
        }
    }

    // 打印测试
    // for (auto& elem : _pageWeight) {
    //     cout << "**********************************" << endl;
    //     cout << elem.first << ": ";
    //     for (auto& w : elem.second) {
    //         cout << w << " ";
    //     }
    //     cout << endl;
    // }
}

// 计算 余弦相似度
void WebPageQuery::getCos() {
    _pageCos.clear();

    for (auto& elem : _pageWeight) {
        int id = elem.first;
        auto pageWeight = elem.second;

        double cos, xy, x, y;
        cos = xy = x = y = 0;

        for (int i = 0; i < pageWeight.size(); ++i) {
            xy += _queryWeight[i] * pageWeight[i];
            x += _queryWeight[i] * _queryWeight[i];
            y += pageWeight[i] * pageWeight[i];
        }
        x = sqrt(x);
        y = sqrt(y);
        cos = xy / (x * y);

        _pageCos.insert({cos, id});
    }

    // 打印测试
    // cout << "余弦相似度: " << endl;
    // for (auto& elem : _pageCos) {
    //     cout << "+++++++++++++++++++++++++++++" << endl;
    //     cout << elem.second << ": " << elem.first << endl;
    // }
}

// 执行查询
string WebPageQuery::getQueryResult() {
    vector<string> queryResult;

    for (auto& elem : _pageCos) {
        string result;
        int id = elem.second;

        result = "Id: " + std::to_string(id) + "\n";
        result += "Title: " + _pageLib[id][1] + "\n";
        result += "Url: " + _pageLib[id][0] + "\n";

    FIXME:
        // 取文章内容, 并去除空白字符
        string desc = _pageLib[id][2];
        regex e("\\s|　|(&nbsp;)|(showPlayer\\([^)]+\\);)");
        desc = regex_replace(desc, e, "");

        // 求所有查询词 第一次出现的位置
        set<int> index;
        for (auto& elem : _queryWords) {
            int tmp = desc.find(elem);
            index.insert(tmp);
        }

        // 判断词距, 确定需要 截取的句子数量
        auto slowIt = index.begin();
        auto fastIt = index.begin();
        fastIt++;
        for (; fastIt != index.end();) {
            if (*fastIt < *slowIt + 60) {
                fastIt = index.erase(fastIt);
            } else {
                slowIt++;
                fastIt++;
            }
        }

        // 从文章中截取需要的句子
        string summary;
        for (auto& elem : index) {
            string tmp;
            if (elem >= 60) {
                tmp = desc.substr(elem - 60, 120);
            } else {
                tmp = desc.substr(0, 120);
            }

            // 1110xxxx 10xxxxxx 10xxxxxx
            // 切除开头不完整 中文字符
            if ((tmp[0] & 0xe0) == 0xe0) {
            } else if ((tmp[1] & 0xe0) == 0xe0) {
                tmp = tmp.substr(1);
            } else {
                tmp = tmp.substr(2);
            }

            // 切除末尾不完整 中文字符
            int size = tmp.size();
            if ((tmp[size - 1] & 0xe0) == 0xe0) {
                tmp = tmp.substr(0, size - 1);
            } else if ((tmp[size - 2] & 0xe0) == 0xe0) {
                tmp = tmp.substr(0, size - 2);
            }

            summary += tmp + "...";
        }
        desc = summary;

        // 将 内容的 查询词高亮
        for (auto& elem : _queryWords) {
            regex e(elem);
            desc = regex_replace(desc, e, "\e[93m" + elem + "\e[39m");
        }

        result += "Summary:" + desc;

        queryResult.push_back(result);
    }
    json retJson;
    if (queryResult.size() != 0) {
        retJson["WebPage"] = queryResult;
        return retJson.dump(-1, ' ', false, json::error_handler_t::ignore);
    } else {
        return "";
    }
}

string WebPageQuery::doQuery(const string& sentence) {
    // 分词, 并把结果存入 _queryWords
    cut(sentence);

    // 计算 待查词的 权重
    getQueryWordsWeight();

    // 求 包含全部查询词的文章Id
    getIntersectionId();

    // 计算 unionId 中各篇文章的 特征向量
    getPageWeight();

    // 计算 余弦相似度
    getCos();

    return getQueryResult();
}
