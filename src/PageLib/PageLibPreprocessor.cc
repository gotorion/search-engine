#include "PageLibPreprocessor.h"

#include <math.h>

#include <chrono>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include "DirScanner.h"
#include "Simhasher.hpp"
#include "configure.h"
PageLibPreprocessor::PageLibPreprocessor()
    : _wordCutter(new SplitToolCppJieba()) {}

void PageLibPreprocessor::doProcess() {
    map<string, string> map1 = Configuration::getInstance()->getConfigMap();
    string path = map1["WEB_PAGE_PATH"];

    DirScanner dir;
    dir.traverse(path);
    vector<string> file = dir.getFiles();
    for (size_t i = 0; i < file.size(); i++) {
        RssReader m;
        m.parseRss(file[i]);
        m.dump(*this);
    }
}

void PageLibPreprocessor::insert(RssItem& tem) {
    WebPage m(tem);
    _pageLib.push_back(m);
}

void PageLibPreprocessor::cutRedundantPages() {
    auto slowIter = _pageLib.begin();
    for (; slowIter != _pageLib.end(); slowIter++) {
        auto fastIter = slowIter;
        fastIter++;
        while (fastIter != _pageLib.end()) {
            if (simhash::Simhasher::isEqual(fastIter->_u, slowIter->_u)) {
                // 删除 元素后 返回 指向 待删除元素下一个元素 的迭代器
                fastIter = _pageLib.erase(fastIter);
            } else {
                fastIter++;
            }
        }
    }
}
void PageLibPreprocessor::createXml() {
    vector<pair<int, int>> file2;
    auto map1 = Configuration::getInstance()->getConfigMap();
    ofstream ofs(map1["WEB_PAGE_LIB"]);
    if (!ofs.good()) {
        cerr << "filename1 error_code\n";
        return;
    }
    auto it = _pageLib.begin();
    int i = 0;
    int begin = 0;
    for (; it != _pageLib.end(); it++) {
        ofs << "<doc>" << endl;
        ofs << "     <id>" << i + 1 << "</docid>" << endl;
        it->_docId = i + 1;
        ofs << "     <url>" << it->_docUr << "</url>" << endl;
        ofs << "     <title>" << it->_docTitle << "</title>" << endl;
        ofs << "     <description>" << it->_docContent << "</description>"
            << endl;
        ofs << "</doc>" << endl;
        i++;
        int nums = ofs.tellp();
        int len = nums - begin;
        file2.push_back(make_pair(begin, len));
        begin = nums;
    }
    ofs.close();
    ofstream ofs2(map1["WEB_OFFSET_LIB"]);
    if (!ofs2.good()) {
        cerr << "filename2 error_code\n";
        return;
    }
    for (i = 0; i < (int)file2.size(); i++) {
        ofs2 << i + 1 << " " << file2[i].first << " " << file2[i].second
             << endl;
    }
    ofs2.close();
}

void PageLibPreprocessor::buildInvertIndex() {
    // 加载 停用词文件, 存入 unorder_map
    unordered_set<string> stopWords;

    auto map1 = Configuration::getInstance()->getConfigMap();
    ifstream ifs(map1["STOP_WORD_PATH"]);
    if (!ifs.good()) {
        perror("buildInvertIndex");
        return;
    }

    string words;
    while (ifs >> words) {
        stopWords.insert(words);
    }

    ifs.close();

    /************************************************/

    // 储存 单词 及 包含该单词的 文档数量
    unordered_map<string, double> pagesCount;

    // 储存 每篇文章 的 词频统计, 其中 vector 索引表示 文章 id, 值为该篇文章的
    // 词频map
    vector<unordered_map<string, double>> pagesWordCount;

    for (auto& page : _pageLib) {
        vector<string> words = _wordCutter->cut(page.getDoc());

        // 遍历分词得到的 vector, 统计每篇文章的词频
        unordered_map<string, double> wordCount;
        for (auto& str : words) {
            // 如果为停用词, 则跳过
            if (stopWords.find(str) != stopWords.end()) {
                continue;
            }

            if (str == " " || str == "\n") {
                continue;
            }

            int nums = wordCount.count(str);
            if (nums == 0) {
                wordCount[str] = 1;
            } else {
                wordCount[str]++;
            }
        }

        // 更新 出现单词的 文档数量
        for (auto& elem : wordCount) {
            int nums = pagesCount.count(elem.first);
            if (nums == 0) {
                pagesCount[elem.first] = 1;
            } else {
                pagesCount[elem.first]++;
            }
        }

        pagesWordCount.push_back(wordCount);
    }

    // 计算 权重 w'
    // 1. TF: 某词 在 文章中 出现的次数
    // 2. DF: 出现 某词 的文档数量
    // 3. IDF = log2(N/(DF+1)) 逆文档频率, N 为 文档总数
    // 4. w = TF * IDF
    // 5. w' = w / sqrt(w1^2 + w2^2 + ... + wn^2) 归一化处理
    for (int i = 0; i < pagesWordCount.size(); ++i) {
        auto& elem = pagesWordCount[i];

        // 记录 w1^2 + w2^2 + ... + wn^2 的和
        double sum = 0;

        // 更新 每篇文章的 词频 为 w权重
        for (auto& pairFreq : elem) {
            double IDF =
                log2(_pageLib.size() / (pagesCount[pairFreq.first] + 1));
            double w = pairFreq.second * IDF;
            sum += w * w;
            pairFreq.second = w;
        }

        // 更新 每篇文章 w权重 为 w'权重 并写入 倒排索引map
        double sqrtW = sqrt(sum);

        for (auto& pairFreq : elem) {
            pairFreq.second /= sqrtW;
            _invertIndexLib[pairFreq.first].push_back(
                make_pair(i + 1, pairFreq.second));
        }
    }

    // ofstream ofs("../../data/invertindex.dat");
    ofstream ofs(map1["WEB_INVERT_LIB"]);
    if (!ofs.good()) {
        perror("invertindex");
        return;
    }

    for (auto& pairFreq : _invertIndexLib) {
        ofs << pairFreq.first << " ";
        for (auto elem : pairFreq.second) {
            ofs << elem.first << " " << elem.second << " ";
        }
        ofs << endl;
    }

    ofs.close();
}
