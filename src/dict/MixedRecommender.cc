#include <queue>

#include "CandidateResult.h"
#include "ChDictionary.h"
#include "EnDictionary.h"
#include "MixedDictionary.h"
#include "Recommender.h"
using std::priority_queue;

string MixedRecommender::recommender(const string& queryWord) {
    // 中英文都要读进来
    MixedDicionaty mixDict;
    vector<pair<string, int>> queryRes(std::move(mixDict.doQuery(queryWord)));
    if (queryRes.size() == 0) {
        return "";
    }
    priority_queue<CandidateResult, vector<CandidateResult>, BetterCandidate>
        candis;
    for (auto& elem : queryRes) {
        int distance = editDistance(queryWord, elem.first);
        candis.emplace(elem.first, elem.second, distance);
    }
    vector<string> ret;
    for (int i = 0; i < 5; ++i) {
        CandidateResult top = candis.top();
        ret.push_back(top._word);
        candis.pop();
    }
    json retJson;
    retJson["Keywords"] = ret;
    return retJson.dump();
}
