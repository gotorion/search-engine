#include <iostream>
#include <nlohmann/json.hpp>
#include <queue>

#include "ChDictionary.h"
#include "Recommender.h"
using std::priority_queue;
using json = nlohmann::json;

string ChRecommender::recommender(const string& queryWord) {
    ChDictionary chDict;
    auto res = std::move(chDict.doQuery(queryWord));
    if (res.size() == 0) {  // 没找到的情况
        return "";
    }
    priority_queue<CandidateResult, vector<CandidateResult>, BetterCandidate>
        candis;
    for (auto& elem : res) {
        int distance = editDistance(queryWord, elem.first);
        candis.emplace(elem.first, elem.second, distance);
    }
    vector<string> ret;
    for (int i = 0; i < 5; ++i) {
        CandidateResult top = candis.top();
        ret.push_back(top._word);
        candis.pop();
    }
    json retJson;
    retJson["Keywords"] = ret;
    return retJson.dump();
}
