#include "MixedDictionary.h"

MixedDicionaty::MixedDicionaty() {}
// 中英文混合查询
vector<pair<string, int>> MixedDicionaty::doQuery(const string& word) {
    vector<string> singleChars(std::move(ChDictionary::cutString(word)));
    // 切分成英文字母和中文汉字
    set<pair<string, int>> ret;
    for (const string& s : singleChars) {
        if (s.size() == 1) {  // 英文字符
            const set<int>& lineNums = _indexTable[s];
            for (auto& line : lineNums) {
                ret.insert(_dict[line - 1]);
            }
        } else {  // 中文字符
            const set<int>& lineNums = _index[s];
            for (auto& line : lineNums) {
                ret.insert(_freq[line - 1]);
            }
        }
    }
    return vector<pair<string, int>>(ret.begin(), ret.end());
}
