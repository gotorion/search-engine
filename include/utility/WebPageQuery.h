#ifndef __WEBPAGEQUERY_H__
#define __WEBPAGEQUERY_H__
#include <functional>
#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include "SplitTool.h"
#include "configure.h"

using std::multimap;
using std::pair;
using std::set;
using std::string;
using std::unordered_map;
using std::unordered_set;
using std::vector;

class WebPageQuery {
   public:
    WebPageQuery();
    string doQuery(const string& sentence);

   private:
    // 加载网页库, 将其存入 _pageLib, 文章id 作为 键, 文章内容(6行)构成的 vector
    // 作为 值
    void loadPageLib();

    // 加载 倒排索引库 文件
    void loadWeightLibrary();

    // 加载停用词
    void loadStopWords();

    // 分词, 并把结果存入 _queryWords
    void cut(const string& sentence);

    // 求 包含全部查询词的文章Id
    void getIntersectionId();

    // 计算查询词的权重, 存入 _queryWeight
    void getQueryWordsWeight();

    // 计算 unionId 中各篇文章的 特征向量
    void getPageWeight();

    // 计算 余弦相似度
    void getCos();

    // 获取查询结果
    string getQueryResult();

   private:
    // 网页库, 文章id : 网页类/vector
    // vector 中保存 url / title / description
    unordered_map<int, vector<string>> _pageLib;

    // 偏移库, 文章id : <起始位置, 文章长度>
    // unordered_map<int, pair<int, int>> _offsetLib;

    // 倒排索引库 单词 : <文章id:权重, ...>
    unordered_map<string, unordered_map<int, double>> _invertIndexLib;

    // df 库, 即记录 单词 及 包含该单词的文章数; 载入倒排索引库时 载入
    unordered_map<string, double> _dfLib;

    // 储存 停用词
    unordered_set<string> _stopWords;

    // 分词对象
    // SplitTool& _wordCutTool;

    // 分词结果
    vector<string> _tmpQueryWords;
    set<string> _queryWords;

    // 包含全部查询词的 文章id, 即 文章Id 的交集
    vector<int> _intersectionId;

    // 储存 查询词的权重
    vector<double> _queryWeight;

    // 储存 各文章对应的 特征向量
    unordered_map<int, vector<double>> _pageWeight;

    // 储存 各文章与查询词的余弦相似度, cos 为键, 文章 id 为值
    multimap<double, int, std::greater<double>> _pageCos;
};

#endif
