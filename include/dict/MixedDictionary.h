#ifndef __MIXEDDICTIONARY_H__
#define __MIXEDDICTIONARY_H__
#include <map>
#include <set>

#include "ChDictionary.h"
#include "Dictionary.h"
#include "EnDictionary.h"

class MixedDicionaty : public EnDictionary, public ChDictionary {
   public:
    MixedDicionaty();
    ~MixedDicionaty() = default;
    vector<pair<string, int>> doQuery(const string& word) override;
};

#endif