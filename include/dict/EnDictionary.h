#ifndef __ENDICTIONARY_H__
#define __ENDICTIONARY_H__
#include <map>
#include <set>
#include <string>
#include <vector>

#include "Dictionary.h"

using std::map;
using std::pair;
using std::set;
using std::string;
using std::vector;

class EnDictionary : public Dictionary {  // 读取本地的词典和索引文件到内存
   public:
    EnDictionary();
    ~EnDictionary() = default;
    vector<pair<string, int>> doQuery(const string& word) override;

   private:
    void getDict();
    void getIndexTable();

   protected:
    string toLower(const string&);

    vector<pair<string, int>> _dict;    // 词典
    map<string, set<int>> _indexTable;  // 索引表
};

#endif