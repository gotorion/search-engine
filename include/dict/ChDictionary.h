#ifndef __CHDICTIONARY_H__
#define __CHDICTIONARY_H__
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

#include "Dictionary.h"

using std::pair;
using std::set;
using std::string;
using std::unordered_map;
using std::vector;
class ChDictionary : public Dictionary {
   public:
    ChDictionary();
    ~ChDictionary() = default;
    vector<pair<string, int>> doQuery(const string& word) override;

   private:
    void getFrequency();
    void getIndex();

   protected:
    vector<string> cutString(const string& str);  // 切分汉字

    vector<pair<string, int>> _freq;
    unordered_map<string, set<int>> _index;
};

#endif